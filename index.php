<?php
$mysqli = new mysqli("localhost", "root", "brasil2020", "cliente");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
 
$query = "SELECT codigo, nome, endereco FROM aluno";

if ($result = $mysqli->query($query)) {

    while ($row = $result->fetch_assoc()) {
        printf ("%s (%s) %s\n", $row["codigo"], $row["nome"], $row["endereco"]);
    }

    $result->close();
}

$mysqli->close();
?>
