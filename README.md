# Objetivo: criar um banco, uma tabela e inserir os dados. Podendo ver a tabela inserida no banco e através de um select mostrar isso no navegador

# Início: 28/10/2020
# Fim: 16/11/2020

# Programas:
     1. Git versão 2 ou superior
     2. PHP versão 5 ou superior
     3. MySQL versão 5 ou superior
     5. Qualquer navegador

# Repositório para clonar Avaliação 2 - https://gitlab.com/guischossler/avaliacao2

# Deixar Ubuntu Server no VirtualBox como bridge, siga os passos na ordem:
     1. Botão direito no server 
     2. Configurações 
     3. Rede 
     4. Conectado a: "Placa em modo Bridge"
     5. "OK"

# Instalação do git:
     sudo apt install git git-core

# Configurar o seu usuário:
     git config --global user.email 'seu_email'
     git config --global user.name 'seu_usuario'

# No /home do seu usuário (no meu caso univates), digite os seguintes comandos para clonar o repositório:
     git clone https://gitlab.com/guischossler/avaliacao2.git
    
# Entrar na pasta:
     cd avaliacao2

# Verificar se o PHP está na versão >= 7.2:
     php --version

# Instalando o php7.2-cli:
Se aparecer a mensagem: "E: could not get lock /var/lib/dpkg/lock-frontend - open .....)
É porque há outro processo usando o "apt", aguarde um pouco e tente novamente depois.
(pressione Y quando o script de instalação pedir):

     sudo apt update

     sudo apt install php7.2-cli
     
# Instalando o MySQL:
     sudo apt install mysql-server
(pressione Y quando o script de instalação pedir)

# Instalando o MySQLi (Driver que usa script PHP para fornecer uma interface com os bancos de dados MySQL):
     sudo apt install php-mysqli
(pressione Y quando o script de instalação pedir)

# Verificar se o MySQL está rodando, se não estiver use "start" no lugar do "status":
     systemctl status mysql.service

# Primeiros passo do MySQL:
## (Responda o script nesta ordem (n, brasil2020, n, n, n, y) após rodar o comando abaixo:
     sudo mysql_secure_installation
    
# Entrando, criando o banco de dados e setar o seu uso:
     sudo mysql -u root -p
     create database cliente
     use cliente
    
# Ver os databases:
     show databases;

# Criando a tabela aluno (coloque cada linha como está descrito abaixo):
     create table aluno(codigo int auto_increment, nome varchar(50) not null, endereco varchar(40) not null, primary key(codigo));

# Inserindo um dado na tabela:
     insert into aluno(nome, endereco) values ("Joaozinho", "Alguma rua aí");

# Ver os dados iseridos na tabela e sair do MySQL:
     select * from aluno;
     exit

# Rodar o php local:
(Use "ifconfig" para ver o ip da sua máquina (NÃO É o 127.0.0.1) CUIDADO COM O DHCP, o ip pode mudar ao reiniciar a máquina):
     ifconfig
Comando para rodar o php local:
     sudo php -S ipdasuamaquina:8080 -t /home/univates/avaliacao2

# Vá para a sua máquina HOST(windows, linux desktop) entre no navegador e na url informe:
     ipdasuamaquina:8080
(feito isso aparecerá o resultado do SQL da tabela aluno no navegador)
